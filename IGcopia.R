#GR2 modificado para el caso de problema de min latencia con multitrip
datos <-read.csv("instancia10.csv",sep=",",header = FALSE)
Q=120
Jornada=480
Demanda <- c(0,10,20,30,10,20,10,20,30,30,30,0)
Qa<-NULL #capacidad disponible del vehiculo
w <-0 # Demanda acumulada
SinCapacidad<-0 #si sincapacidad es igual a 0 significa que el vehiculo todavía tiene capacidad disponible posible, 
#si es igual a 1 significa que el vehiculo no tiene capacidad disponible factible, es decir, 
#digamos que Qa=4 y que solo quedan disponibles usuarios con demanda mayor que 4, entonces 
#Sincapacidad debe ser igual a 1.
N=10 #numero de clientes
#el depot son el 1 y el 12
Sa=2:(N+1)#nodos que faltan ser asignados
SP<- c(1)#la ruta inicia en el depot
Depots<-2#cantidad totales de depot, es decir el depot y sus copias, cantidad max de 1 que pueden aparecer en la solución final
#quitamos el primer y  último cliente porque para esta instancia solo hay 2 depots (MODIFICAR PARA CUANDO HAYA MÁS)
DemandaClientes <- Demanda[-1]
DemandaClientes <- DemandaClientes[-(length(DemandaClientes))]
tablaDemandaClientes<-data.frame(Sa,DemandaClientes)

Solucion <- NULL

#######Información para el GR2 #######################
TablaCPD <-NULL #esta tabla me ayudara a guardar la info del cliente, mejor pos en SP y valor de delta.
clientes <- 1:(N+Depots)
mejorPos <- rep(-1,(N+Depots))
valorM1 <- rep(-100,(N+Depots))
TablaCPD <- data.frame(clientes, mejorPos, valorM1)
DeltaMin <- NULL
DeltaMax <- NULL
Alfa <- 0.3 #porcentaje de aceptación
RCL <-NULL
#####################################################

#*****************************************************************
#                     *CONSTRUCTIVO *
#*****************************************************************
while (length(Sa)>0) { #Sa distinto del vacio, hay al menos un elemento en Sa
  SP <-c(1)#Secuencia parcial aux, cada que se tiene terminada una secuencia SP se concatena a la solucion final, cada SP inicia con 1, ya que 1 representa el depot
  SinCapacidad <-0 #cada que inicia una nueva secuencia, hay capacidad en el vehiculo
  w<-0 #cuando inicia una secuencia la demanda acumulada es 0 
  while (SinCapacidad ==0) {#condición equivalente en GR2 por Sa distinto de vacio
    Qa<- Q-w #capacidad disponible en el vehiculo
    TablaAux <- subset(tablaDemandaClientes,tablaDemandaClientes$DemandaClientes<=Qa)
    Sa1 <- as.numeric(TablaAux$Sa)#clientes factibles, no sobrrepasan la capacidad disponible
    ###GR2###
    #aqui se calcula el mejor punto de inserción para todo elemento de sa
    K=as.numeric(length(SP))
    if(K>=2){
      for (i in Sa1) {
        Delta<-NULL
        m1<-NULL
        Suma<-0
        q<-NULL
        
        for(j in 2:(K+1)){
          if(j==2){
            m1 <- ((K+1)*datos[1,i])+ (K*(datos[i,SP[2]]-datos[1,SP[2]]))
            q <-2
            Suma <-0
          }else if(j==(K+1)){
            Suma <- Suma+ datos[SP[K-1],SP[K]]
            Delta <- Suma + datos[SP[K],i]
            if(m1>Delta){
              m1 <- Delta
              q <- j
            }
          }else{
            Suma <- Suma+datos[SP[j-1],SP[j]]
            Delta <- Suma + ((K-j+2)*datos[SP[j-1],i])+ ((K-j+1)*(datos[i,SP[j]]-datos[SP[j-1],SP[j]]))
            if(m1>Delta){
              m1 <- Delta
              q <- j
            }
          }
        }
        TablaCPD$mejorPos[i]<-q
        TablaCPD$valorM1[i]<-m1
      }
    }else{
      for(i in Sa1){
        TablaCPD$mejorPos[i]<-2
        TablaCPD$valorM1[i]<- datos[1,i]
      }
    }
    #aqui se empiza el proceso de creación de la lista restringida de candidatos
    SubTablaCPD <- subset.data.frame(TablaCPD,TablaCPD$clientes %in% Sa1)
    Indicemin <- as.numeric(which.min(SubTablaCPD$valorM1)) #que indice tiene el min valor
    MinVal <- SubTablaCPD[Indicemin,3] #cual es el min valor
    Indicemax <- as.numeric(which.max(SubTablaCPD$valorM1)) #que indice tiene el max valor
    MaxVal <- SubTablaCPD[Indicemax,3] #cual es el max valor
    
    for(s in Sa1){
      if(as.numeric(TablaCPD[s,3])< MinVal+(Alfa*MaxVal)){
        RCL<- c(RCL,s)
      }
    }
    lonRCL<- as.numeric(length(RCL))
    elegidoaux <- sample(lonRCL,1)#elegimos aleatoriamente un elemento de la RCL
    elegido <- as.numeric(RCL[elegidoaux]) #que cliente se agrega a SP
    RCL<-NULL
    Pos <-as.numeric(TablaCPD[elegido,2]) #cual es la mejor posición donce se insertará el cliente elegido
    if(K==1 || Pos==K+1){#añadimos el cliente elegido a SP
      SP <- c(SP,elegido)
    }else{
      #Pos <-as.numeric(TablaCPD[elegido,2]) #cual es la mejor posición donce se insertará el cliente elegido
      LI <- SP[1:Pos-1]#Lado izquierdo
      LD <- SP[Pos:K]#Lado derecho
      SP <- c(LI,elegido,LD)
    }
    g<-as.numeric(which(Sa==elegido))#quitamos el cliente elegido de Sa
    g1<-as.numeric(which(Sa1==elegido))
    g2<-as.numeric(which(tablaDemandaClientes$Sa==elegido))
    Sa <- Sa[-g]
    Sa1 <- Sa1[-g1]
    #termina GR2
    w<-w+ as.numeric(tablaDemandaClientes$DemandaClientes[g2]) #actuliza demanda acumulada
    tablaDemandaClientes<-tablaDemandaClientes[-g2,]
    
    TablaDemandaAux <- tablaDemandaClientes[tablaDemandaClientes$Sa %in% Sa,]#filtra los elementos que siguen disponibles
    TablaDemCapacidad <- TablaDemandaAux[TablaDemandaAux$DemandaClientes <= Q-w,]#filtra de los disponibles cuales respetan la capacidad disponible
    if(nrow(TablaDemCapacidad)>=1){
      SinCapacidad<-0
    }else{
      SinCapacidad<-1 #ya no hay clientes con demanda factible 
    }
  }
  Solucion <- c(Solucion,SP)
}

#calcula el valor de la latencia total de la solucion final
VO<-0
Auxiliar<-N+Depots-1
Coeficiente <- N+Depots
for(o in 1:Auxiliar){
  Nodo1 <- Solucion[o]
  Nodo2 <- Solucion[o+1]
  CalLat<- (Coeficiente-o)*datos[Nodo1,Nodo2]
  VO<- VO+CalLat
}

print("Secuencia:")
print(Solucion)
print("Latencia Total:")
print(VO)

############################################################################
#************************ DESTRUCCION *************************************#
############################################################################
cuantos<- sample(2:N,1)#cuantos nodos se van a eliminar de la sol
NodosAEliminar<- sample(2:(N+1),cuantos)#cuales son esos nodos a eliminar
PosNodosAEliminar <- NULL
for(i in NodosAEliminar){
  posEnsol<- which(Solucion==i) #que posicion tiene el nodo a eliminar en la solucion
  PosNodosAEliminar<-c(PosNodosAEliminar,posEnsol)
}
Sparcial<- Solucion[-PosNodosAEliminar]#Solucion parcial para ingresar al constructivo

##########################################################################
#         *****************CONSTRUCCION*****************
##########################################################################

PosDeLosDepots <- which(Sparcial==1) #posición de los depots en la Sparcial para poder separar en multitrips
NumSubCaminos <- as.numeric(length(PosDeLosDepots)) #cantidad de subcaminos-trips
Sol <- NULL #solucion recosntruida
for(jj in 1:NumSubCaminos){#rellenando los subcaminos con el constructivo
  A1<- as.numeric(PosDeLosDepots[jj])#donde inicia el subcamino
  if(jj != NumSubCaminos){#separa de la Sparcial cada subcamino
    A2<- as.numeric(PosDeLosDepots[jj+1])-1 #donde termina el subcamino
    SP<- Sparcial[A1:A2]
  }else{#es el último subcamino
    SP <- Sparcial[A1:as.numeric(length(Sparcial))]
  }
  ####aqui inicia GR2 modificado####-------------------------------------
  Sa<- sort(NodosAEliminar)
  DemandaClientes <- Demanda[Sa]
  tablaDemandaClientes<-data.frame(Sa,DemandaClientes)
  
  TablaCPD <-NULL #esta tabla me ayudara a guardar la info del cliente, mejor pos en SP y valor de delta.
  clientes <- 1:(N+Depots)
  mejorPos <- rep(-1,(N+Depots))
  valorM1 <- rep(-100,(N+Depots))
  TablaCPD <- data.frame(clientes, mejorPos, valorM1)
  DeltaMin <- NULL
  DeltaMax <- NULL
  Alfa <- 0.3 #porcentaje de aceptación
  RCL <-NULL
  
  #Demanda acumulada de SP
  w<-0
  LongitudSP <- length(SP)
  if(LongitudSP>1){
    for(u in 2:LongitudSP){
    w <- w+Demanda[SP[u]]
    }
  }
  
  #verificar si hay nodos factibles para añadir a Sol
  TablaDemandaAux <- tablaDemandaClientes[tablaDemandaClientes$Sa %in% Sa,]#filtra los elementos que siguen disponibles
  TablaDemCapacidad <- TablaDemandaAux[TablaDemandaAux$DemandaClientes <= Q-w,]#filtra de los disponibles cuales respetan la capacidad disponible
  if(nrow(TablaDemCapacidad)>=1){
    SinCapacidad<-0
  }else{
    SinCapacidad<-1 #ya no hay clientes con demanda factible 
  }
  
  while (length(Sa)>0 & SinCapacidad ==0) { #Sa distinto del vacio, hay al menos un elemento en Sa
      Qa<- Q-w #capacidad disponible en el vehiculo
      TablaAux <- subset(tablaDemandaClientes,tablaDemandaClientes$DemandaClientes<=Qa)
      Sa1 <- as.numeric(TablaAux$Sa)#clientes factibles, no sobrrepasan la capacidad disponible
      ###GR2###
      #aqui se calcula el mejor punto de inserción para todo elemento de sa
      K=as.numeric(length(SP))
      if(K>=2){
        for (i in Sa1) {
          Delta<-NULL
          m1<-NULL
          Suma<-0
          q<-NULL
          
          for(j in 2:(K+1)){
            if(j==2){
              m1 <- ((K+1)*datos[1,i])+ (K*(datos[i,SP[2]]-datos[1,SP[2]]))
              q <-2
              Suma <-0
            }else if(j==(K+1)){
              Suma <- Suma+ datos[SP[K-1],SP[K]]
              Delta <- Suma + datos[SP[K],i]
              if(m1>Delta){
                m1 <- Delta
                q <- j
              }
            }else{
              Suma <- Suma+datos[SP[j-1],SP[j]]
              Delta <- Suma + ((K-j+2)*datos[SP[j-1],i])+ ((K-j+1)*(datos[i,SP[j]]-datos[SP[j-1],SP[j]]))
              if(m1>Delta){
                m1 <- Delta
                q <- j
              }
            }
          }
          TablaCPD$mejorPos[i]<-q
          TablaCPD$valorM1[i]<-m1
        }
      }else{
        for(i in Sa1){
          TablaCPD$mejorPos[i]<-2
          TablaCPD$valorM1[i]<- datos[1,i]
        }
      }
      #aqui se empiza el proceso de creación de la lista restringida de candidatos
      SubTablaCPD <- subset.data.frame(TablaCPD,TablaCPD$clientes %in% Sa1)
      Indicemin <- as.numeric(which.min(SubTablaCPD$valorM1)) #que indice tiene el min valor
      MinVal <- SubTablaCPD[Indicemin,3] #cual es el min valor
      Indicemax <- as.numeric(which.max(SubTablaCPD$valorM1)) #que indice tiene el max valor
      MaxVal <- SubTablaCPD[Indicemax,3] #cual es el max valor
      
      for(s in Sa1){
        if(as.numeric(TablaCPD[s,3])< MinVal+(Alfa*MaxVal)){
          RCL<- c(RCL,s)
        }
      }
      lonRCL<- as.numeric(length(RCL))
      elegidoaux <- sample(lonRCL,1)#elegimos aleatoriamente un elemento de la RCL
      elegido <- as.numeric(RCL[elegidoaux]) #que cliente se agrega a SP
      RCL<-NULL
      Pos <-as.numeric(TablaCPD[elegido,2]) #cual es la mejor posición donde se insertará el cliente elegido
      if(K==1 || Pos==K+1){#añadimos el cliente elegido a SP
        SP <- c(SP,elegido)
      }else{
        #Pos <-as.numeric(TablaCPD[elegido,2]) #cual es la mejor posición donde se insertará el cliente elegido
        LI <- SP[1:Pos-1]#Lado izquierdo
        LD <- SP[Pos:K]#Lado derecho
        SP <- c(LI,elegido,LD)
      }
      g<-as.numeric(which(Sa==elegido))#quitamos el cliente elegido de Sa
      g1<-as.numeric(which(Sa1==elegido))
      g2<-as.numeric(which(tablaDemandaClientes$Sa==elegido))
      g3 <- as.numeric(which(NodosAEliminar==elegido))
      Sa <- Sa[-g]
      Sa1 <- Sa1[-g1]
      NodosAEliminar<-NodosAEliminar[-g3]
      #termina GR2
      w<-w+ as.numeric(tablaDemandaClientes$DemandaClientes[g2]) #actuliza demanda acumulada
      tablaDemandaClientes<-tablaDemandaClientes[-g2,]
      
      TablaDemandaAux <- tablaDemandaClientes[tablaDemandaClientes$Sa %in% Sa,]#filtra los elementos que siguen disponibles
      TablaDemCapacidad <- TablaDemandaAux[TablaDemandaAux$DemandaClientes <= Q-w,]#filtra de los disponibles cuales respetan la capacidad disponible
      if(nrow(TablaDemCapacidad)>=1){
        SinCapacidad<-0
      }else{
        SinCapacidad<-1 #ya no hay clientes con demanda factible 
      }
  }
  Sol <- c(Sol,SP)
}
#calcula el valor de la latencia total de la solucion final
VObj<-0
Auxiliar<-N+Depots-1
Coeficiente <- N+Depots
for(o in 1:Auxiliar){
  Nodo1 <- Sol[o]
  Nodo2 <- Sol[o+1]
  CalLat<- (Coeficiente-o)*datos[Nodo1,Nodo2]
  VObj<- VObj+CalLat
}

print("Secuencia:")
print(Sol)
print("Latencia Total:")
print(VObj)
#######################################################################################


###**********************************************************************************
###                   ------  --       IG       ------  --
###**********************************************************************************
s0 <- Solucion #solucion inicial
MejorSol <- s0 #mejor solucion
VoMejorSol <- VO #valor objetivo de la mejor solucion
Contador <- 0 #auxiliar para condicion de parada del IG
M <-6 #Condicion de Parada del IG, si se llega a M iteraciones sin mejora se detiene el algoritmo

while(Contador < M){
  
  #********************************* DESTRUCCION *************************************#
  cuantos<- sample(2:N,1)#cuantos nodos se van a eliminar de la sol
  NodosAEliminar<- sample(2:(N+1),cuantos)#cuales son esos nodos a eliminar
  PosNodosAEliminar <- NULL
  for(i in NodosAEliminar){
    posEnsol<- which(Solucion==i) #que posicion tiene el nodo a eliminar en la solucion
    PosNodosAEliminar<-c(PosNodosAEliminar,posEnsol)
  }
  Sparcial<- Solucion[-PosNodosAEliminar]#Solucion parcial para ingresar al constructivo
  #************************************************************************************
  
  
  #********************************* CONSTRUCCION *********************************#
  PosDeLosDepots <- which(Sparcial==1) #posición de los depots en la Sparcial para poder separar en multitrips
  NumSubCaminos <- as.numeric(length(PosDeLosDepots)) #cantidad de subcaminos-trips
  Sol <- NULL #solucion recosntruida
  for(jj in 1:NumSubCaminos){#rellenando los subcaminos con el constructivo
    A1<- as.numeric(PosDeLosDepots[jj])#donde inicia el subcamino
    if(jj != NumSubCaminos){#separa de la Sparcial cada subcamino
      A2<- as.numeric(PosDeLosDepots[jj+1])-1 #donde termina el subcamino
      SP<- Sparcial[A1:A2]
    }else{#es el último subcamino
      SP <- Sparcial[A1:as.numeric(length(Sparcial))]
    }
    ####aqui inicia GR2 modificado####-------------------------------------
    Sa<- sort(NodosAEliminar)
    DemandaClientes <- Demanda[Sa]
    tablaDemandaClientes<-data.frame(Sa,DemandaClientes)
    
    TablaCPD <-NULL #esta tabla me ayudara a guardar la info del cliente, mejor pos en SP y valor de delta.
    clientes <- 1:(N+Depots)
    mejorPos <- rep(-1,(N+Depots))
    valorM1 <- rep(-100,(N+Depots))
    TablaCPD <- data.frame(clientes, mejorPos, valorM1)
    DeltaMin <- NULL
    DeltaMax <- NULL
    Alfa <- 0.3 #porcentaje de aceptación
    RCL <-NULL
    
    #Demanda acumulada de SP
    w<-0
    LongitudSP <- length(SP)
    if(LongitudSP>1){
      for(u in 2:LongitudSP){
        w <- w+Demanda[SP[u]]
      }
    }
    
    #verificar si hay nodos factibles para añadir a Sol
    TablaDemandaAux <- tablaDemandaClientes[tablaDemandaClientes$Sa %in% Sa,]#filtra los elementos que siguen disponibles
    TablaDemCapacidad <- TablaDemandaAux[TablaDemandaAux$DemandaClientes <= Q-w,]#filtra de los disponibles cuales respetan la capacidad disponible
    if(nrow(TablaDemCapacidad)>=1){
      SinCapacidad<-0
    }else{
      SinCapacidad<-1 #ya no hay clientes con demanda factible 
    }
    
    while (length(Sa)>0 & SinCapacidad ==0) { #Sa distinto del vacio, hay al menos un elemento en Sa
      Qa<- Q-w #capacidad disponible en el vehiculo
      TablaAux <- subset(tablaDemandaClientes,tablaDemandaClientes$DemandaClientes<=Qa)
      Sa1 <- as.numeric(TablaAux$Sa)#clientes factibles, no sobrrepasan la capacidad disponible
      ###GR2###
      #aqui se calcula el mejor punto de inserción para todo elemento de sa
      K=as.numeric(length(SP))
      if(K>=2){
        for (i in Sa1) {
          Delta<-NULL
          m1<-NULL
          Suma<-0
          q<-NULL
          
          for(j in 2:(K+1)){
            if(j==2){
              m1 <- ((K+1)*datos[1,i])+ (K*(datos[i,SP[2]]-datos[1,SP[2]]))
              q <-2
              Suma <-0
            }else if(j==(K+1)){
              Suma <- Suma+ datos[SP[K-1],SP[K]]
              Delta <- Suma + datos[SP[K],i]
              if(m1>Delta){
                m1 <- Delta
                q <- j
              }
            }else{
              Suma <- Suma+datos[SP[j-1],SP[j]]
              Delta <- Suma + ((K-j+2)*datos[SP[j-1],i])+ ((K-j+1)*(datos[i,SP[j]]-datos[SP[j-1],SP[j]]))
              if(m1>Delta){
                m1 <- Delta
                q <- j
              }
            }
          }
          TablaCPD$mejorPos[i]<-q
          TablaCPD$valorM1[i]<-m1
        }
      }else{
        for(i in Sa1){
          TablaCPD$mejorPos[i]<-2
          TablaCPD$valorM1[i]<- datos[1,i]
        }
      }
      #aqui se empiza el proceso de creación de la lista restringida de candidatos
      SubTablaCPD <- subset.data.frame(TablaCPD,TablaCPD$clientes %in% Sa1)
      Indicemin <- as.numeric(which.min(SubTablaCPD$valorM1)) #que indice tiene el min valor
      MinVal <- SubTablaCPD[Indicemin,3] #cual es el min valor
      Indicemax <- as.numeric(which.max(SubTablaCPD$valorM1)) #que indice tiene el max valor
      MaxVal <- SubTablaCPD[Indicemax,3] #cual es el max valor
      
      for(s in Sa1){
        if(as.numeric(TablaCPD[s,3])< MinVal+(Alfa*MaxVal)){
          RCL<- c(RCL,s)
        }
      }
      lonRCL<- as.numeric(length(RCL))
      elegidoaux <- sample(lonRCL,1)#elegimos aleatoriamente un elemento de la RCL
      elegido <- as.numeric(RCL[elegidoaux]) #que cliente se agrega a SP
      RCL<-NULL
      Pos <-as.numeric(TablaCPD[elegido,2]) #cual es la mejor posición donde se insertará el cliente elegido
      if(K==1 || Pos==K+1){#añadimos el cliente elegido a SP
        SP <- c(SP,elegido)
      }else{
        #Pos <-as.numeric(TablaCPD[elegido,2]) #cual es la mejor posición donde se insertará el cliente elegido
        LI <- SP[1:Pos-1]#Lado izquierdo
        LD <- SP[Pos:K]#Lado derecho
        SP <- c(LI,elegido,LD)
      }
      g<-as.numeric(which(Sa==elegido))#quitamos el cliente elegido de Sa
      g1<-as.numeric(which(Sa1==elegido))
      g2<-as.numeric(which(tablaDemandaClientes$Sa==elegido))
      g3 <- as.numeric(which(NodosAEliminar==elegido))
      Sa <- Sa[-g]
      Sa1 <- Sa1[-g1]
      NodosAEliminar<-NodosAEliminar[-g3]
      #termina GR2
      w<-w+ as.numeric(tablaDemandaClientes$DemandaClientes[g2]) #actuliza demanda acumulada
      tablaDemandaClientes<-tablaDemandaClientes[-g2,]
      
      TablaDemandaAux <- tablaDemandaClientes[tablaDemandaClientes$Sa %in% Sa,]#filtra los elementos que siguen disponibles
      TablaDemCapacidad <- TablaDemandaAux[TablaDemandaAux$DemandaClientes <= Q-w,]#filtra de los disponibles cuales respetan la capacidad disponible
      if(nrow(TablaDemCapacidad)>=1){
        SinCapacidad<-0
      }else{
        SinCapacidad<-1 #ya no hay clientes con demanda factible 
      }
    }
    Sol <- c(Sol,SP)
  }
  #calcula el valor de la latencia total de la solucion final
  VObj<-0
  Auxiliar<-N+Depots-1
  Coeficiente <- N+Depots
  for(o in 1:Auxiliar){
    Nodo1 <- Sol[o]
    Nodo2 <- Sol[o+1]
    CalLat<- (Coeficiente-o)*datos[Nodo1,Nodo2]
    VObj<- VObj+CalLat
  }
  
  print("Secuencia:")
  print(Sol)
  print("Latencia Total:")
  print(VObj)
  #************************************************************************************
  
  
  #***************************** Criterio de aceptacion ******************************#
  if(VObj< VO){
    MejorSol <-Sol
    VO<- VObj
    Solucion <-MejorSol
  }else{
    Contador <- Contador+1
  }
}





